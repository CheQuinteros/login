<?php
    Class GestorPerfilController{
        public function  loginController(){
            if (isset($_POST["userName"]) || isset($_POST["userPass"]  )) {

                if (preg_match('/^[a-zA-Z0-9]*$/', $_POST["userName"] ) ||
                     preg_match('/^[a-zA-Z0-9]*$/', $_POST["userPass"] )) {

                        $datosController =  $_POST["userName"];
                        $respuesta = GestorPerfilModel::loginModel($datosController, "user");

                        $encriptar = crypt($_POST["userPass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
                        
                        if ($respuesta["pass"] == $encriptar) {
                            session_start();
                            $_SESSION["validar"] = true;

                            $respuesta2 = GestorPerfilModel::loginDatosModel($datosController, "user");

                            GestorPerfilModel::actualizarEstadoPerfilModel($datosController, "user");
                            
                            foreach ($respuesta2 as $row => $item) {
                                $_SESSION["nombre"] = $item["nombre"];
                                $_SESSION["apellido"] = $item["apellido"];
                                $_SESSION["email"] = $item["email"];
                                $_SESSION["rol"] = $item["rol"];
                                $_SESSION["foto"] = $item["foto"];
                                $_SESSION["id"] = $item["id"];
                            }

                            header("location:inicio");
                        }else{
                            echo '
                            <div class="alert alert-danger" role="alert">
                                <strong>Contraeña incorrescta!</strong>
                            </div> 
                            ';
                            
                        }
                }else{
                    echo '
                    <div class="alert alert-danger" role="alert">
                        <strong>No caracteres especiales</strong>
                    </div>
                    ';
                }
                     
            }else{
                echo '
                <div class="alert alert-danger" role="alert">
                    <strong>Un campo esta vacio</strong>
                </div>
                ';
            }
        }

        public function vistaPerfilesController(){
            $respuesta = GestorPerfilModel::vistaPerfilesModel( "user");
            foreach ($respuesta as $row => $item) {

                if ($item["rol"] == 0) {
                    $rol = "Administrador";
                }else{
                    $rol = "Editor";

                }
                echo '
                <tr>
                    <td scope="row">'.$item["nombre"].'</td>
                    <td>'.$item["apellido"].'</td>
                    <td>'.$item["email"].'</td>
                    <td>'.$rol.'</td>
                    <td>'.$item["userName"].'</td>
                    <td>
                        <button type="button" class="btn btn-primary buttonEditar" data-toggle="modal" data-target="#modelId2" ><i class="fas fa-pen-alt"></i></button>
                        <a href="index.php?action=perfiles&deleteP='.$item["id"].'" class="btn btn-danger buttonEliminar"> <i class="fas fa-trash-alt    "></i> </a>
                    </td>
                </tr>
                ';
            }
        }
        public function guardarPerfilController(){

            if (isset($_POST["userNombre"]) && 
                isset($_POST["userApellido"]) &&
                isset($_POST["userPass"]) &&
                isset($_POST["userEmail"]) &&
                isset($_POST["userRol"]) &&
                isset($_POST["user"])) {

                  

                $ruta = "";

                if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["userNombre"] ) &&
                    preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["userApellido"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["userPass"] ) &&
                    preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/' , $_POST["userEmail"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["userRol"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["user"] )) {
                    
                        if(isset($_FILES["userFoto"]["tmp_name"])){
                            $imagen = $_FILES["userFoto"]["tmp_name"];
                            $aleatorio = mt_rand(100, 999);
                            $ruta = "Views/image/user/user".$aleatorio.".jpg";
                            $origen = imagecreatefromjpeg($imagen);
                            $destino = imagecrop($origen, ["x"=>0, "y"=>0, "width"=>100, "height"=>100]);
                            imagejpeg($destino, $ruta);
                        }
                        if($ruta == ""){
                            $ruta = "Views/image/user/user.jpg";	
                        }

                        $encriptar = crypt($_POST["userPass"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                        $datosController = array('nombre' => $_POST["userNombre"],
                                                'apellido' => $_POST["userApellido"],
                                                'user' => $_POST["user"],
                                                'email' => $_POST["userEmail"],
                                                'pass' => $encriptar,
                                                'rol' => $_POST["userRol"],
                                                'foto' => $ruta);

                        $respuesta = GestorPerfilModel::guardarPerfilModel($datosController, "user");

                        if ($respuesta == "ok") {

                            

                            echo '<script>
                            swal({
                                title: "ok",
                                text: "Perfil Registrado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                            });
                                
                            </script>';
                        }else{
                            echo $respuesta;
                        }

                    
                }
                

            }
        }

        public function eliminarPerfilesController(){
            if (isset($_GET["deleteP"])) {
                $datosController = $_GET["deleteP"];

                $respuesta2 = GestorPerfilModel::traerFotoPerfilesModel($datosController, "user");
                unlink($respuesta2["foto"]);

                $respuesta = GestorPerfilModel::eliminarPerfilesModel($datosController, "user");
                if ($respuesta == "ok") {
                    echo '<script>
                                
                                window.location = "perfiles";
                            </script>';
                   
             
                }else{
                    echo $respuesta;
                }
            }
        }

        public function editarPerfilController(){
            if (isset($_POST["userNombreEditar"]) &&
                isset($_POST["userApellidoEditar"]) &&
                isset($_POST["userPassEditar"]) &&
                isset($_POST["userEmailEditar"]) &&
                isset($_POST["userRolEditar"]) &&
                isset($_POST["userEditar"])) {

                    $ruta = "";

                if (preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["userNombreEditar"] ) &&
                    preg_match('/^[a-zA-Z0-9 ]*$/', $_POST["userApellidoEditar"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["userPassEditar"] ) &&
                    preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/' , $_POST["userEmailEditar"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["userRolEditar"] ) &&
                    preg_match('/^[a-zA-Z0-9]*$/', $_POST["userEditar"] )) {
                    
                        if(isset($_FILES["userFotoEditar"]["tmp_name"]) ){
                            $imagen = $_FILES["userFotoEditar"]["tmp_name"];
                            $aleatorio = mt_rand(100, 999);
                            $ruta = "Views/image/user/user".$aleatorio.".jpg";
                            $origen = imagecreatefromjpeg($imagen);
                            $destino = imagecrop($origen, ["x"=>0, "y"=>0, "width"=>100, "height"=>100]);
                            imagejpeg($destino, $ruta);
                        }
                        if ( $ruta == "") {
                            $ruta = $_POST["userFotoEditarOculto"];
                        }
                        if($_POST["userFotoEditarOculto"] != "Views/image/user/user.jpg" && $ruta != "" ){
                            unlink($_POST["userFotoEditarOculto"]);
                        }

                        $encriptar = crypt($_POST["userPassEditar"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');

                        $datosController = array('nombre' => $_POST["userNombreEditar"],
                                                'apellido' => $_POST["userApellidoEditar"],
                                                'user' => $_POST["userEditar"],
                                                'email' => $_POST["userEmailEditar"],
                                                'pass' => $encriptar,
                                                'rol' => $_POST["userRolEditar"],
                                                'id' => $_POST["userIDEditar"],
                                                'foto' => $ruta);

                        $respuesta = GestorPerfilModel::editarPerfilModel($datosController, "user");

                        if ($respuesta == "ok") {
                            echo '<script>
                            swal({
                                title: "ok",
                                text: "Perfil Registrado Correctamente!",
                                type: "success",
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                              },
                              function(isConfirm) {
                                if (isConfirm) {
                                    window.location = "perfiles";
                                } 
                              });
                                
                            </script>';
                        }else{
                            echo $respuesta;
                        }

                    
                }
            }
        }

        public function estadoCerrarController($userController){
            $respuesta = GestorPerfilModel::actualizarEstadoCerrarPerfilModel($userController, "user");
           echo $respuesta;
               
           
        }

        public function imprimerPerfilesController(){
            $respuesta = GestorPerfilModel:: imprimirPerfilesModel("user");
            return $respuesta;
        }
    }


?>