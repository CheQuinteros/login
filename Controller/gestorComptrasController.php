<?php

    Class GestorComprasController{
        public function traerProveedoresController(){
            $respuesta = GestorComprasModel::traerProveedoresModel("proveedores");
            foreach ($respuesta as $row => $item) {
                echo '   
                    <option value="'.$item["id"].'">'.$item["nombre"].'</option>
                
                ';
            }
        }
        public function traerProductosParaCompraController($datoController){
            $respuesta = GestorComprasModel::traerProductosParaCompraModel($datoController, "productos");
            foreach ($respuesta as $row => $item) {
                echo '  
                 
                    <option value="'.$item["id"].'">'.$item["nombre"].'</option>
                
                ';
            }
        }
        public function traerInstructoresController($datoController){
            $respuesta = GestorComprasModel::traerInstructoresModel($datoController, "instructores");
            foreach ($respuesta as $row => $item) {
                echo '  
                     
                    <option value="'.$item["id"].'">'.$item["nombre"].'</option>
                
                ';
            }
        }
    }

?>