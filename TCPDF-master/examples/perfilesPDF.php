<?php
include "../../Controller/gestorPerfilesController.php";
include "../../Model/gestorPerfilesModel.php";

Class RepotesPerfiles{

public function imprimirPerfiles(){

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Unicaes CRI');
$pdf->SetTitle('Reporte de Perfiles');
$pdf->SetSubject('29 de octubre del 2020');
$pdf->SetKeywords('Sistema de ex-alumnos');

// set default header data
$pdf->SetHeaderData("UNICAES.png", 20, 'UNICAES CRI', "Reporte de perfiles\n29 de octubre del 2019\nwww.unicaescri.com.sv", array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));


// // set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// $pdf->setPrintHeader(false);
// $pdf->setPrintFooter(false);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$respuesta = GestorPerfilController::imprimerPerfilesController();
$html = <<<EOD
<br>
<br>

<table>
		<tr style="width: 635px; background-color: black;">
			<th style="color: white; text-align: center; width: 150px;">Nombre</th>
			<th style="color: white; text-align: center; width: 150px;">Apellido</th>
			<th style="color: white; text-align: center; width: 160px;">Email</th>
			<th style="color: white; text-align: center; width: 100px;">User</th>
			<th style="color: white; text-align: center; width: 75px;">rol</th>
		</tr>

</table>
EOD;

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('dejavusans', '', 10, '', true);

foreach ($respuesta as $row => $item) {
if ($item["rol"] == 1) {
	$rol = "Admin";
}else{
	$rol = "Edit";
}
$html2 = <<<EOD
<table>
		<tr style="width: 635px;">
			<td class="celda" style="width: 150px;"> $item[nombre]</td>
			<td class="celda"style="width: 150px;"> $item[apellido]</td>
			<td class="celda"style="width: 160px;"> $item[email]</td>
			<td class="celda" style="width: 100px;"> $item[userName]</td>
			<td class="celda" style="width: 75px;"> $rol</td>
		</tr>
</table>
<style>
	.celda{
		border: 1px solid black;
		height: 20px;
		
	}
</style>

EOD;
$pdf->writeHTMLCell(0, 0, '', '', $html2, 0, 1, 0, true, '', true);

}

// Print text using writeHTMLCell()

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('perfilesPDF.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+


}
}

$ejecutar = new RepotesPerfiles();
$ejecutar->imprimirPerfiles();

?>