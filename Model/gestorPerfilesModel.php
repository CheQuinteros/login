<?php

require_once "conexcion.php";

Class GestorPerfilModel{
    public function loginModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("SELECT pass FROM $tabla WHERE userName = '$datoModel' ");
        $stmp -> execute();
        return $stmp->fetch();
        $stmp->close();
    }
    public function loginDatosModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("SELECT id, nombre, apellido, email, userName, rol, foto FROM $tabla WHERE userName = '$datoModel' ");
        $stmp -> execute();
        return $stmp->fetchAll();
        $stmp->close();
    }
    public function vistaPerfilesModel($tabla){
        $stmp = Conexcion::Conectar()->prepare("SELECT id, nombre, apellido, email, userName, rol, foto FROM $tabla WHERE estado = 0");
        $stmp -> execute();
        return $stmp->fetchAll();
        $stmp->close();
    }
    public function guardarPerfilModel($datosModel, $tabla){
        
        $stmp = Conexcion::Conectar()->prepare("INSERT INTO $tabla (nombre, apellido, email, userName, pass, foto, rol) VALUES (:nombre, :apellido, :email, :userName, :pass, :foto, :rol)");

        $stmp->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
        $stmp->bindParam(":apellido", $datosModel["apellido"], PDO::PARAM_STR);
        $stmp->bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
        $stmp->bindParam(":userName", $datosModel["user"], PDO::PARAM_STR);
        $stmp->bindParam(":foto", $datosModel["foto"], PDO::PARAM_STR);
        $stmp->bindParam(":pass", $datosModel["pass"], PDO::PARAM_STR);
        $stmp->bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);

        if ($stmp -> execute()) {
            return "ok";
        }else{
            return "error";
        }
        
        
        $stmp->close();
    }
    public function eliminarPerfilesModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("DELETE FROM $tabla WHERE id = :id");
        $stmp->bindParam(":id", $datoModel, PDO::PARAM_INT);
        if ($stmp -> execute()) {
            return "ok";
        }else{
            return "error";
        }
        $stmp->close();

    }

    public function traerFotoPerfilesModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("SELECT foto FROM $tabla WHERE id = :id ");
        $stmp->bindParam(":id", $datoModel, PDO::PARAM_INT);        
        $stmp -> execute();
        return $stmp->fetch();
        $stmp->close();
    }
    public function editarPerfilModel($datosModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("UPDATE $tabla SET nombre = :nombre, apellido = :apellido, email = :email, userName = :userName, pass = :pass, foto = :foto, rol=:rol WHERE id = :id");

        $stmp->bindParam(":nombre", $datosModel["nombre"], PDO::PARAM_STR);
        $stmp->bindParam(":apellido", $datosModel["apellido"], PDO::PARAM_STR);
        $stmp->bindParam(":email", $datosModel["email"], PDO::PARAM_STR);
        $stmp->bindParam(":userName", $datosModel["user"], PDO::PARAM_STR);
        $stmp->bindParam(":foto", $datosModel["foto"], PDO::PARAM_STR);
        $stmp->bindParam(":pass", $datosModel["pass"], PDO::PARAM_STR);
        $stmp->bindParam(":rol", $datosModel["rol"], PDO::PARAM_INT);
        $stmp->bindParam(":id", $datosModel["id"], PDO::PARAM_INT);

        if ($stmp -> execute()) {
            return "ok";
        }else{
            return "error";
        }
        
        
        $stmp->close();
    }

    public function actualizarEstadoPerfilModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("UPDATE $tabla SET estado = 1 WHERE nombre = '$datoModel'");
        if ($stmp -> execute()) {
            return "ok";
        }else{
            return "error";
        }
        $stmp->close();
    }

    public function actualizarEstadoCerrarPerfilModel($datoModel, $tabla){
        $stmp = Conexcion::Conectar()->prepare("UPDATE $tabla SET estado = 0 WHERE nombre = '$datoModel'");
        if ($stmp -> execute()) {
            return "ok";
        }else{
            return "error";
        }
        $stmp->close();
    }

    public function imprimirPerfilesModel($tabla){
        $stmp = Conexcion::Conectar()->prepare("SELECT nombre, apellido, email, userName, rol, foto FROM $tabla");
        $stmp -> execute();
        return $stmp->fetchAll();
        $stmp->close();
    }
}

?>