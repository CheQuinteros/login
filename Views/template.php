<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Dashboard Template · Bootstrap</title>

  
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="Views/css/bootstrap.min.css">
    <link rel="stylesheet" href="Views/css/sweetalert.css">
    <script src="Views/js/sweetalert.min.js"></script>

    <link rel="stylesheet" href="Views/fonts/fontawesome/css/brands.css">
    <link rel="stylesheet" href="Views/fonts/fontawesome/css/fontawesome.css">
    <link rel="stylesheet" href="Views/fonts/fontawesome/css/solid.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  
    

    <script defer src="Views/fonts/fontawesome/js/brands.js"></script>
    <script defer src="Views/fonts/fontawesome/js/solid.js"></script>
    <script defer src="Views/fonts/fontawesome/js/fontawesome.js"></script>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="Views/css/dashboard.css" rel="stylesheet">
    <link href="Views/css/Chart.min.css" rel="stylesheet">
</head>
  <body>
  
    <?php 
      //Aqui se pintan todos los modules a cargar segun la ruta 
      $enlaces = new GestorEnlacesController();
      $enlaces -> enlacesController();
      
    ?>


    <script src="Views/js/jquery.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <script src="Views/js/bootstrap.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="Views/js/Chart.min.js"></script>
    <script src="Views/js/dashboard.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

    <script src="Views/js/perfiles.js"></script>
    <script src="Views/js/compras.js"></script>
  </body>
</html>


