
    $('#tablaperfiles').DataTable({
        responsive: {
            orthogonal: 'responsive'
        },
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
        
    });



    $("#tablaperfiles").on("click", "a.buttonEliminar", function(e) {
        let respuesta = confirm("Estas seguro de eliminar!");
        if (!respuesta) {
            e.preventDefault();
        }
    });
    $("#tablaperfiles").on("click", "button.buttonEditar", function(e) {
        var nombre;
        var apellido;
        var email;
        var user;
        var padre = ($(this).parent().parent());
        $.each(padre, function () { 
            nombre = $(this).find("td").eq(0).html();
            apellido = $(this).find("td").eq(1).html();
            email = $(this).find("td").eq(2).html();
            user = $(this).find("td").eq(4).html();
            href = $(this).find("td").find("a").attr("href");
        });
        
        $("#userNombreEditar").val(nombre);
        $("#userApellidoEditar").val(apellido);
        $("#userEmailEditar").val(email);
        $("#userEditar").val(user);
        $("#userFotoEditarOculto").val($(".fotoPerfil").attr("src"));
        $("#userIDEditar").val(href.substr(34));
   
    });

    //VALIDAR FORM DE REGISTRO

    function validarRegistrojs(){
        var expresion = /^[a-zA-Z0-9 ]*$/;

        var nombre = document.querySelector("#userNombre").value;
        var nombre_input = document.querySelector("#userNombre");

        if (!expresion.test(nombre) || nombre.length <= 0 ) {
            
            nombre_input.style.border = "1px solid red";
            $("#nuevoPerfilAlert").css( "display" , "block");
            return false;
        }else{
            nombre_input.style.border = "";
            $("#nuevoPerfilAlert").css( "display" , "none");
        }

        return false;
    }
