$(document).ready(function () {
    $("#seleccion").on("change", "#estadoProducto", function(){
        // console.log($(this).val());

        var dato = $(this).val();
        var datos = new FormData();
        datos.append('valor', dato);

        $.ajax({
            method: "POST",
            url: "Views/modules/AjaxCompras.php",
            data: datos,
            contentType: false,
            cache: false,
            processData: false,
            success: function (respuesta) {
                
                document.getElementById("productosCompra").innerHTML = '<option>--Selecionar--</option>'+respuesta;
            }
        });
    })


    // vefiricando cheked
    $(".form-check-input").click(function(){

        var dato = $(this).val();

        var datos = new FormData();
        datos.append('valor2', dato);

        $.ajax({
            method: "POST",
            url: "Views/modules/AjaxCompras.php",
            data: datos,
            contentType: false,
            cache: false,
            processData: false,
            success: function (respuesta) {
                
                document.getElementById("productosCompra").innerHTML = '<option>--Selecionar--</option>'+respuesta;
            }
        });


    })
});