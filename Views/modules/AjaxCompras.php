<?php
require_once "../../Controller/gestorComptrasController.php";
require_once "../../Model/gestorComprasModel.php";

Class AjaxCompra{
    public $datos;
    public function traerProductosParaCompraAjax(){
        $datosAjax = $this->datos;
        $respuesta = GestorComprasController::traerProductosParaCompraController($datosAjax);
        echo $respuesta;
    }

    public $turno;
    public function traerInstructoresAjax(){
        $datosAjax = $this->turno;
        $respuesta = GestorComprasController::traerInstructoresController($datosAjax);
        echo $respuesta;
    }

}

if (isset($_POST["valor"])) {
    $traerProduc = new AjaxCompra();
    $traerProduc -> datos = $_POST["valor"];
    $traerProduc -> traerProductosParaCompraAjax();
}
if (isset($_POST["valor2"])) {
    $traerProduc = new AjaxCompra();
    $traerProduc -> turno = $_POST["valor2"];
    $traerProduc -> traerInstructoresAjax();
}



?>