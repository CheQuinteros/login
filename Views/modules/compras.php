<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:login");
  exit();
}
require_once 'cabezote.php';
?>
<div class="container-fluid">
    <div class="row">
        <?php require_once 'menu.php'; ?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
            <h1 class="text-center">Compras</h1>
            
            <div class="form-group">
              <label for="proveedoresCompra"></label>
              <select class="form-control" name="proveedoresCompra" id="proveedoresCompra">
                <option>--Selecionar--</option>
                <?php
                    $traerPro = new GestorComprasController();
                    $traerPro -> traerProveedoresController();
                ?>
              </select>
            </div>

            <div class="form-group" id="seleccion">
              <label for="estadoProducto"></label>
              <select class="form-control" name="estadoProducto" id="estadoProducto">
                <option>--Selecionar--</option>
                <option value="usado">usado</option>
                <option value="nuevo">nuevo</option>
              </select>
            </div>

           
          
           <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="turnoInstructor" id="Radio1" value="Mañana">             
            <label class="form-check-label" for="Radio1">Mañana</label>
           </div>
           <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="turnoInstructor" id="Radio2" value="Tarde">             
            <label class="form-check-label" for="Radio2">Tarde</label>
           </div>

            <div class="form-group">
              <label for="productosCompra"></label>
              <select class="form-control" name="productosCompra" id="productosCompra">
                <option>--Selecionar--</option>
                
              </select>
            </div>

        </main>
    </div>
</div>