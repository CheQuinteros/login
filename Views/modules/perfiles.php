<?php
session_start();
if (!$_SESSION["validar"]) {
  header("location:login");
  exit();
}
require_once 'cabezote.php';
?>
<div class="container-fluid">
    <div class="row">
        <?php require_once 'menu.php'; ?>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4"> 
            <h1 class="text-center">Perfiles</h1>
            <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg m-2" data-toggle="modal" data-target="#modelId">
 Agregar
</button>
           <div class="table-responsive">
           <table id="tablaperfiles" class="table table-hover table-sm table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>UserName</th>
                        <th>accion</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                    $vistaP = new GestorPerfilController();
                    $vistaP -> vistaPerfilesController();
                    $vistaP -> eliminarPerfilesController();
                   ?>
            
                </tbody>
            </table>
           </div>
           <a href="TCPDF-master/examples/perfilesPDF.php" class="btn btn-warning">Imprimir</a>
        </main>
        
    </div>
    
</div>


<!-- Modal -->
<div class="modal fade" id="modelId2" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Perfil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
              <form method="POST" enctype="multipart/form-data">
              <input type="hidden" name="userIDEditar" id="userIDEditar">
              <input type="hidden" name="userFotoEditarOculto" id="userFotoEditarOculto">

                <div class="form-group">
                  <label for="userNombreEditar">Nombre</label>
                  <input type="text" name="userNombreEditar" id="userNombreEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userApellidoEditar">Apellido</label>
                  <input type="text" name="userApellidoEditar" id="userApellidoEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userEditar">User</label>
                  <input type="text" name="userEditar" id="userEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userPassEditar">Password</label>
                  <input type="password" name="userPassEditar" id="userPassEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userEmailEditar">Email</label>
                  <input type="email" name="userEmailEditar" id="userEmailEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userFotoEditar">Email</label>
                  <input type="file" name="userFotoEditar" id="userFotoEditar" class="form-control" placeholder="" aria-describedby="helpId">
                </div>
                <div class="form-group">
                  <label for="userRolEditar"></label>
                  <select  class="form-control" name="userRolEditar" id="userRolEditar">
                    <option >--Seleccionar--</option>
                    <option value="0">Administrador</option>
                    <option value="1">Editor</option>
                  </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                
<?php
    $nuevo = new GestorPerfilController();
    $nuevo->editarPerfilController();

    ?>

            </form>

            </div>
         
 

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo Perfil</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="alert alert-danger" role="alert" id="nuevoPerfilAlert" style="display:none">
              <strong>Los campos en rojo no se aceptan.</strong>
            </div>
            <div class="modal-body">
              <form method="POST" enctype="multipart/form-data" onsubmit="return validarRegistrojs()">
                <div class="form-group">
                  <label for="userNombre">Nombre</label>
                  <input type="text" name="userNombre" id="userNombre" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="userApellido">Apellido</label>
                  <input type="text" name="userApellido" id="userApellido" class="form-control"  >
                </div>
                <div class="form-group">
                  <label for="user">User</label>
                  <input type="text" name="user" id="user" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="userPass">Password</label>
                  <input type="password" name="userPass" id="userPass" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="userEmail">Email</label>
                  <input type="email" name="userEmail" id="userEmail" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="userFoto">Email</label>
                  <input type="file" name="userFoto" id="userFoto" class="form-control" >
                </div>
                <div class="form-group">
                  <label for="userRol"></label>
                  <select  class="form-control" name="userRol" id="userRol">
                    <option >--Seleccionar--</option>
                    <option value="0">Administrador</option>
                    <option value="1">Editor</option>
                  </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                
            <?php
                $editar = new GestorPerfilController();
                $editar->guardarPerfilController();

                ?>

            </form>

            </div>
         
 

        </div>
    </div>
</div>
